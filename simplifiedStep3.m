%% %%%%%% FILAMENT RECOGNITION  %%%%%%

% !!! Set img_fill as your binarized image first
dist_thresh = 3;

% Mask with Bulky Structures - Distance Map to black background
img_dist = bwdist(~img_fill);
img_dist = img_dist > dist_thresh; % applies dist.-map threshold to create 
                                   % a mask with bulky structures
% thickness for reconstructing bulky structures
thick_ = ceil(dist_thresh)+1;
img_dist = bwmorph(img_dist,'thicken',thick_);    

img_thin = bitand(img_fill,~img_dist); % mask is applied

% Size Filter
% removes objects with less than certain amount of pixels (e.g. 5)
img_thin = (bwareaopen(img_thin, 5));

clear img_dist dist_mask

%% RRG computation
dim_imgs = size(img_thin); % dimensions X and Y of the image 
img_spine = img_thin;
[L,N] = bwlabel(img_spine,8); % isolates each object present in image

obj_centroids = regionprops(L, 'Centroid'); % centroids of each object
obj_diameters = regionprops(L, 'EquivDiameter'); % equiv.diameter of " "
obj_pixelsList_spine = regionprops(L,'PixelIdxList'); % list of white pixels 
                                                % composing each obj.
RG = zeros(N,1); %vector containing values R.R.G. for each obj.
RRGMap = L;

for i = 1:N    
    
    % coordinates (x,y) of each pixel composing the object
    [pixels_y,pixels_x] = ind2sub(dim_imgs,obj_pixelsList_spine(i).PixelIdxList);   
    
    % computes distance between each pixel and the obj.'s centroid
    sum_x = sum( (pixels_x - obj_centroids(i,1).Centroid(1,1)).^2 );
    sum_y = sum( (pixels_y - obj_centroids(i,1).Centroid(1,2)).^2 );

    % computes the moments in each axis
    M2x = sum_x/length(pixels_x);
    M2y = sum_y/length(pixels_y);
    
    % computes the R.R.G.
    RG(i) = sqrt(M2x + M2y) / (obj_diameters(i).EquivDiameter ./2); 
    
    % constructs a image map of RRG values
    RRGMap(obj_pixelsList_spine(i).PixelIdxList) = RG(i);          
end
