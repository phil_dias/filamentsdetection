function [paths,lengths] = reprocessBranch(paths,obj_Image,lengths,dim_imgs,offX_Obj2Img,offY_Obj2Img)
%% Isolate Skeleton's Spine
removedBranches = obj_Image - paths;
[LL,NN] = bwlabel(removedBranches);

subobj_pixelsList = regionprops(LL,'PixelIdxList'); % list of white pixels 
%                                                     % composing each obj.
subobj_Images = regionprops(LL,'Image');% portraits containing each single obj.
subdim_imgs = size(obj_Image); % dimensions X and Y of the image 

mtxBranches = regionprops(LL,'Area');
lengthBranches = [mtxBranches.Area];
idx2reprocess = find(lengthBranches > 30);
    
for it_branch = 1:length(idx2reprocess)
    sub_endpoints = bwmorph(subobj_Images(idx2reprocess(it_branch),1).Image, 'endpoints'); % list of endpoints
    [sub_y,sub_x] = find(sub_endpoints); % coordinates (x,y) of each endpoint
    
    % detection of the four suitable endpoints:       
    sub_difX = abs(sub_x-size(subobj_Images(idx2reprocess(it_branch),1).Image,2)); % dist. between endpoints and X limit
    sub_difY = abs(sub_y-size(subobj_Images(idx2reprocess(it_branch),1).Image,1)); % dist. between endpoints and Y limit
    
    if ~isempty(sub_difX) && ~isempty(sub_difY)
        [~,list_idx(1)] = max(sub_difX(:)); % closest to the left border
        [~,list_idx(2)] = min(sub_difX(:)); % closest to the right border
        [~,list_idx(3)] = max(sub_difY(:)); % closest to the top
        [~,list_idx(4)] = min(sub_difY(:)); % closest to the bottom
        
    else
        [sub_y,sub_x] = find(subobj_Images(idx2reprocess(it_branch),1).Image);
         
        list_idx(1) = max(sub_y(:)); % closest to the left border
        list_idx(2) = min(sub_x(:)); % closest to the right border
        list_idx(3) = max(sub_y(:)); % closest to the top
        list_idx(4) = min(sub_x(:)); % closest to the bottom
    end
    
    % vector containing: 
    % maximal distance between endpoints | starting endpoint | farthest (final) endpoint 
    sub_maxD = [0 0 0];
    
    for endp = 1:4
        sub_D = bwdistgeodesic(subobj_Images(idx2reprocess(it_branch),1).Image, sub_x(list_idx(endp)),sub_y(list_idx(endp)),'quasi-euclidean');
        sub_D(isinf(sub_D)) = 0; % attributes 0 to pixels containing infinite value
        sub_D(isnan(sub_D)) = 0; % attributes 0 to pixels containing non-available value
        
        if sub_maxD(1) < max(sub_D(:)) % updates maximal distance between endpoints
           sub_maxD = [max(sub_D(:)) list_idx(endp) find(sub_D == max(sub_D(:)),1)]; 
           sub_copyD = sub_D; % retains a copy of the geodesic transform related to 
                      % the endpoint that is the starting point for this maximal distance
        end
        
    end     

    [sub_Y,sub_X] = ind2sub(size(sub_D),sub_maxD(3)); % coordinates (x,y) of the farthest endpoint
    
    % geodesic transform starting from this farthest endpoint
    sub_D2 = bwdistgeodesic(subobj_Images(idx2reprocess(it_branch),1).Image, double(sub_X),double(sub_Y), 'quasi-euclidean');
    sub_D = sub_copyD + sub_D2; % addition of distances calculated in both directions
    sub_D = round(sub_D * 8) / 8; % rounding values

%     sub_lengths(i,1) = min(sub_D(:)); % update vector containing length of each spine
    
    sub_D(isnan(sub_D)) = inf;
    sub_paths = imregionalmin(sub_D); % detection of shortest path based on region mi
    
    % maps each subobject from its portrait back to the whole object portrait 
    [objY,objX] = ind2sub(subdim_imgs,subobj_pixelsList(idx2reprocess(it_branch),1).PixelIdxList(1,1));
    [imY,imX] = find(subobj_Images(idx2reprocess(it_branch),1).Image > 0,1);
    [pY,pX] = find(sub_paths > 0);     
    
    offsetX = objX-imX; offsetY = objY-imY; 
    
    %remapped into object portrait
    X_branchInObjIdx = pX+offsetX;
    Y_branchInObjIdx = pY+offsetY;
    
    id_white = sub2ind(subdim_imgs,Y_branchInObjIdx,X_branchInObjIdx);
    paths(id_white) = 1;

    paths = imclose(paths,strel('square',3));
    paths = bwmorph(paths,'thin');  
       
    % remapped the branch back to the original image  
    id_BranchInImg = sub2ind(dim_imgs,Y_branchInObjIdx+offY_Obj2Img,X_branchInObjIdx+offX_Obj2Img);
    lengths(id_BranchInImg) = min(sub_D(:));
end
